System.register(['angular2/core', 'rxjs/add/operator/map', 'rxjs/add/operator/debounceTime', 'rxjs/add/operator/distinctUntilChanged', 'rxjs/add/operator/switchMap', './input-debounce.component', './import.component'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, input_debounce_component_1, import_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (_1) {},
            function (_2) {},
            function (_3) {},
            function (_4) {},
            function (input_debounce_component_1_1) {
                input_debounce_component_1 = input_debounce_component_1_1;
            },
            function (import_component_1_1) {
                import_component_1 = import_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(zone) {
                    var _this = this;
                    this.fonts = [];
                    this.fontTotal = 0;
                    this.search = '';
                    this.fontSize = 16;
                    this.activeMenu = '';
                    this.previewText = 'The quick brown fox jumps over the lazy dog. 1234567890';
                    this.openType = [
                        { 'name': 'Swash', 'className': 'swsh' },
                        { 'name': 'Contextual', 'className': 'calt' },
                        { 'name': 'Stylistic', 'className': 'salt' }
                    ];
                    this.checkOpenType = {};
                    this.pagination = 20;
                    this.viewMode = 'unmax';
                    this.collection = 'all';
                    this.showSpinner = false;
                    this.customCollections = [];
                    this.newCollection = '';
                    this.marginTop = 0;
                    this.fontPagination = {
                        start: 0,
                        end: this.pagination
                    };
                    this.zone = zone;
                    this.updateFontSizeStyles();
                    if (typeof require != "undefined") {
                        this.ipc = require('electron').ipcRenderer;
                        this.ipc.on('message', function (event, message) { return _this.addAllFonts(message); });
                        this.ipc.on('stylesheet', function (event, message) { return _this.addStyles(message); });
                        this.ipc.on('collections', function (event, message) { return _this.addCollections(message); });
                    }
                    else {
                        this.addAllFonts([{ path: '/Library/Fonts/Arial.ttf',
                                postscriptName: 'Arial',
                                className: 'arial',
                                family: 'Arial',
                                style: 'Regular',
                                type: 'ttf',
                                weight: 400,
                                width: 5,
                                italic: false,
                                monospace: false },
                            { path: '/Library/Fonts/Arial Bold.ttf',
                                postscriptName: 'Arial Bold',
                                className: 'arialbold',
                                family: 'Arial',
                                style: 'Bold',
                                type: 'ttf',
                                weight: 700,
                                width: 5,
                                italic: false,
                                monospace: false }]);
                    }
                }
                AppComponent.prototype.buildOpenTypeList = function () {
                    for (var i = 1; i <= 7; i++) {
                        this.openType.push({ 'name': 'Alternate style ' + i, 'className': 'ss0' + i });
                    }
                };
                AppComponent.prototype.buildCheckBoxes = function () {
                    for (var _i = 0, _a = this.openType; _i < _a.length; _i++) {
                        var record = _a[_i];
                        this.checkOpenType[record.className] = false;
                    }
                };
                AppComponent.prototype.fontFiltering = function (searchTerm) {
                    var fonts;
                    if (searchTerm && searchTerm != '') {
                        var response = [];
                        for (var _i = 0, _a = this.fonts; _i < _a.length; _i++) {
                            var record = _a[_i];
                            record.hide = false;
                            var searchIn = record.postscriptName.toLowerCase();
                            var searchFor = searchTerm.toLowerCase();
                            if (this.collection == "all") {
                                if (searchIn.match(searchFor))
                                    response.push(record);
                            }
                            else {
                                if (searchIn.match(searchFor) && ((record.collections.indexOf(this.collection) > -1) || (this.collection == 'fav' && record.favourite)))
                                    response.push(record);
                            }
                        }
                        fonts = response;
                    }
                    else {
                        var response = [];
                        for (var _b = 0, _c = this.fonts; _b < _c.length; _b++) {
                            var record = _c[_b];
                            record.hide = false;
                            if (this.collection == 'all')
                                response.push(record);
                            else if (this.collection == 'fav' && record.favourite)
                                response.push(record);
                            else if (record.collections.indexOf(this.collection) > -1)
                                response.push(record);
                        }
                        fonts = response;
                    }
                    fonts.sort(function (a, b) {
                        var aName = a.postscriptName.toLowerCase();
                        var bName = b.postscriptName.toLowerCase();
                        if (aName < bName)
                            return -1;
                        if (aName > bName)
                            return 1;
                        return 0;
                    });
                    this.fontTotal = fonts.length;
                    this.filteredFonts = fonts.slice(this.fontPagination.start, this.fontPagination.end);
                };
                AppComponent.prototype.changeCollection = function (collection) {
                    if (collection == this.collection)
                        return;
                    this.collection = collection;
                    this.fontPagination.end = this.pagination;
                    this.pinned = false;
                    this.marginTop = 0;
                    document.getElementsByClassName('font-listing')[0].scrollTop = 0;
                    this.fontFiltering(this.search);
                };
                AppComponent.prototype.addCollection = function () {
                    if (this.newCollection == '')
                        return;
                    this.customCollections.push(this.newCollection);
                    if (this.ipc) {
                        this.ipc.send('collection.add', this.newCollection);
                    }
                    this.newCollection = '';
                };
                AppComponent.prototype.removeCollection = function (collection) {
                    var index = this.customCollections.indexOf(collection);
                    if (index > -1) {
                        this.customCollections.splice(index, 1);
                        if (this.ipc) {
                            this.ipc.send('collection.remove', collection);
                        }
                    }
                };
                AppComponent.prototype.addCollections = function (collections) {
                    var _this = this;
                    this.zone.run(function () {
                        _this.customCollections = collections;
                    });
                };
                AppComponent.prototype.addFont = function (font) {
                    this.fonts.push(font);
                    this.fontFiltering(this.search);
                };
                AppComponent.prototype.addAllFonts = function (fonts) {
                    var _this = this;
                    this.zone.run(function () {
                        _this.fonts = fonts;
                        _this.fontFiltering(_this.search);
                    });
                };
                AppComponent.prototype.addFontToCollection = function (collection) {
                    if (!this.dragging)
                        return;
                    var info = {
                        fontClass: this.dragging.className,
                        fontCollection: collection,
                        add: true
                    };
                    if (this.ipc) {
                        this.ipc.send('font-info', info);
                    }
                    this.dragging.collections.push(collection);
                };
                AppComponent.prototype.removeFontInCollection = function (font) {
                    var info = {
                        fontClass: font.className,
                        fontCollection: this.collection,
                        add: false
                    };
                    if (this.ipc) {
                        this.ipc.send('font-info', info);
                    }
                    var index = font.collections.indexOf(this.collection);
                    if (index > -1) {
                        font.collections.splice(index, 1);
                        font.hide = true;
                    }
                };
                AppComponent.prototype.onDragOver = function (evt) {
                    evt.preventDefault();
                };
                AppComponent.prototype.onDrop = function (evt, collection) {
                    evt.preventDefault();
                    this.addFontToCollection(collection);
                };
                AppComponent.prototype.onDragStart = function (dragging) {
                    this.dragging = dragging;
                };
                AppComponent.prototype.searchUpdate = function (evt) {
                    this.search = evt;
                    this.fontPagination.end = this.pagination;
                    this.fontFiltering(evt);
                };
                AppComponent.prototype.addStyles = function (message) {
                    var _this = this;
                    this.zone.run(function () {
                        _this.stylesheet = message;
                    });
                };
                AppComponent.prototype.updateFontSizeStyles = function () {
                    this.fontSizeStyles = "<style>\n\t\t\t.font-listing .template{\n\t\t\t\tfont-size: " + this.fontSize + "px;\n\t\t\t}\n\t\t</style>";
                };
                AppComponent.prototype.toggleMenuButton = function (button) {
                    if (this.activeMenu == button)
                        this.activeMenu = '';
                    else
                        this.activeMenu = button;
                };
                AppComponent.prototype.pinFont = function (font) {
                    if (this.pinned && this.pinned.className == font.className) {
                        this.pinned = false;
                        this.marginTop = 0;
                    }
                    else {
                        this.pinned = font;
                        var els = document.getElementsByClassName(font.className);
                        if (els.length) {
                            var height = els[0].parentElement.offsetHeight;
                            this.marginTop = height;
                        }
                    }
                };
                AppComponent.prototype.onScroll = function (evt) {
                    var _this = this;
                    var bodyHeight = evt.target.scrollHeight;
                    var scrollTop = evt.target.scrollTop;
                    var windowHeight = window.innerHeight;
                    if (this.fontTotal > this.fontPagination.end) {
                        if ((bodyHeight - scrollTop) < windowHeight) {
                            this.fontPagination.end += this.pagination;
                            this.fontFiltering(this.search);
                            this.showSpinner = true;
                            setTimeout(function () {
                                _this.showSpinner = false;
                            }, 500);
                        }
                    }
                };
                AppComponent.prototype.previewUpdate = function (evt) {
                    this.previewText = evt;
                };
                AppComponent.prototype.setViewMode = function (mode) {
                    this.viewMode = mode;
                    if (this.ipc) {
                        this.ipc.send(mode);
                    }
                };
                AppComponent.prototype.updateFavourite = function (font) {
                    font.favourite = !font.favourite;
                    if (font.favourite)
                        this.ipc.send('favourite.add', font.postscriptName);
                    else
                        this.ipc.send('favourite.remove', font.postscriptName);
                    if (!font.favourite && this.collection == 'fav') {
                        font.hide = true;
                    }
                };
                AppComponent.prototype.ngOnInit = function () {
                    this.buildOpenTypeList();
                    this.buildCheckBoxes();
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        templateUrl: 'tpl/home.html',
                        directives: [input_debounce_component_1.InputDebounceComponent, import_component_1.ImportComponent]
                    }), 
                    __metadata('design:paramtypes', [core_1.NgZone])
                ], AppComponent);
                return AppComponent;
            })();
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map