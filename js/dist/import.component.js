System.register(['angular2/core', 'angular2/http'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1;
    var ImportComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            ImportComponent = (function () {
                function ImportComponent(http) {
                    this.html = '';
                    this.http = http;
                }
                ImportComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.http.get(this.url)
                        .map(function (res) { return res.text(); })
                        .subscribe(function (html) { return _this.html = html; });
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', String)
                ], ImportComponent.prototype, "url", void 0);
                ImportComponent = __decorate([
                    core_1.Component({
                        selector: 'import',
                        viewProviders: [http_1.HTTP_PROVIDERS],
                        template: '<div [innerHTML]="html"></div>',
                    }), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], ImportComponent);
                return ImportComponent;
            })();
            exports_1("ImportComponent", ImportComponent);
        }
    }
});
//# sourceMappingURL=import.component.js.map