System.register(['angular2/core', 'rxjs/Rx'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, Rx_1;
    var InputDebounceComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (Rx_1_1) {
                Rx_1 = Rx_1_1;
            }],
        execute: function() {
            InputDebounceComponent = (function () {
                function InputDebounceComponent(elementRef) {
                    var _this = this;
                    this.elementRef = elementRef;
                    this.delay = 300;
                    this.type = 'text';
                    this.value = new core_1.EventEmitter();
                    var eventStream = Rx_1.Observable.fromEvent(elementRef.nativeElement, 'keyup')
                        .map(function () { return _this.inputValue; })
                        .debounceTime(this.delay)
                        .distinctUntilChanged();
                    eventStream.subscribe(function (input) { return _this.value.emit(input); });
                }
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', String)
                ], InputDebounceComponent.prototype, "placeholder", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Number)
                ], InputDebounceComponent.prototype, "delay", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', String)
                ], InputDebounceComponent.prototype, "type", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', String)
                ], InputDebounceComponent.prototype, "inputValue", void 0);
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', core_1.EventEmitter)
                ], InputDebounceComponent.prototype, "value", void 0);
                InputDebounceComponent = __decorate([
                    core_1.Component({
                        selector: 'input-debounce',
                        template: '<input [type]="type" [placeholder]="placeholder" [(ngModel)]="inputValue">'
                    }), 
                    __metadata('design:paramtypes', [core_1.ElementRef])
                ], InputDebounceComponent);
                return InputDebounceComponent;
            })();
            exports_1("InputDebounceComponent", InputDebounceComponent);
        }
    }
});
//# sourceMappingURL=input-debounce.component.js.map