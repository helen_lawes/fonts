import {Component,View, NgZone} from 'angular2/core';
import {Control} from 'angular2/common';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import {InputDebounceComponent} from './input-debounce.component';
import {ImportComponent} from './import.component';

interface Prop {
	name: string;
	className: string;
}
interface Pagination {
	start: number;
	end: number;
}

@Component({
	selector:'my-app',
	templateUrl: 'tpl/home.html',
	directives:[InputDebounceComponent,ImportComponent]
})
export class AppComponent {
	public fonts:any = [];
	public filteredFonts: Array<any>;
	public fontTotal:number = 0;
	public search:string = '';
	public fontSize:number = 16;
	public fontSizeStyles:string;
	public activeMenu:string = '';
	public previewText:string = 'The quick brown fox jumps over the lazy dog. 1234567890';
	public openType:Prop[] = [
		{'name':'Swash','className':'swsh'},
		{'name':'Contextual','className':'calt'},
		{'name':'Stylistic','className':'salt'}
	];
	public checkOpenType:any = {};
	public stylesheet:string;
	private zone: NgZone;
	private pagination:number = 20;
	public viewMode:string = 'unmax';
	public collection:string = 'all';
	public showSpinner:boolean = false;
	public customCollections:Array<string> = [];
	public newCollection:string = '';
	public dragging:any;
	public pinned:any;
	public marginTop:number = 0;

	private fontPagination:Pagination = {
		start: 0,
		end: this.pagination
	};
	private ipc:any;

	buildOpenTypeList() {
		for(var i = 1; i <= 7; i++){
			this.openType.push({'name':'Alternate style '+i,'className':'ss0'+i});
		}
	}
	buildCheckBoxes() {
		for( let record of this.openType ){
			this.checkOpenType[record.className] = false;
		}
	}
	fontFiltering(searchTerm: string){
		var fonts;
		if(searchTerm && searchTerm != ''){
			let response = [];
			for( let record of this.fonts ){
				record.hide = false;
				var searchIn = record.postscriptName.toLowerCase();
				var searchFor = searchTerm.toLowerCase();
				if( this.collection == "all"){
					if( searchIn.match( searchFor ) ) response.push(record);
				} else {
					if( searchIn.match( searchFor ) && ((record.collections.indexOf(this.collection) > -1) || (this.collection == 'fav' && record.favourite )) ) response.push(record);
				}

			}
			fonts = response;
		} else {
			let response = [];
			for(let record of this.fonts){
				record.hide = false;
				if(this.collection == 'all') response.push(record);
				else if(this.collection == 'fav' && record.favourite) response.push(record);
				else if(record.collections.indexOf(this.collection)>-1) response.push(record);
			}
			fonts = response;
		}
		fonts.sort((a,b) => {
			var aName = a.postscriptName.toLowerCase();
			var bName = b.postscriptName.toLowerCase();
			if(aName < bName) return -1;
			if(aName > bName) return 1;
			return 0;
		});
		this.fontTotal = fonts.length;
		this.filteredFonts = fonts.slice(this.fontPagination.start,this.fontPagination.end);
	}
	changeCollection(collection){
		if(collection==this.collection) return;
		this.collection = collection;
		this.fontPagination.end = this.pagination;
		this.pinned = false;
		this.marginTop = 0;
		document.getElementsByClassName('font-listing')[0].scrollTop = 0;
		this.fontFiltering(this.search);
	}
	addCollection(){
		if(this.newCollection == '') return;
		this.customCollections.push(this.newCollection);
		if(this.ipc){
			this.ipc.send('collection.add',this.newCollection);
		}
		this.newCollection = '';
	}
	removeCollection(collection: string){
		var index = this.customCollections.indexOf(collection);
		if(index>-1){
			this.customCollections.splice(index,1);
			if(this.ipc){
				this.ipc.send('collection.remove',collection);
			}
		}
	}
	addCollections(collections: any){
		this.zone.run(() => {
			this.customCollections = collections;
		});
	}
	addFont(font: any){
		this.fonts.push(font);
		this.fontFiltering(this.search);
	}
	addAllFonts(fonts: any) {
		this.zone.run(() => {
			this.fonts = fonts;
			this.fontFiltering(this.search);
		});
	}
	addFontToCollection(collection: string){
		if(!this.dragging) return;
		var info = {
			fontClass: this.dragging.className,
			fontCollection: collection,
			add: true
		};
		if(this.ipc){
			this.ipc.send('font-info',info);
		}
		this.dragging.collections.push(collection);
	}
	removeFontInCollection(font: any){
		var info = {
			fontClass: font.className,
			fontCollection: this.collection,
			add: false
		};
		if(this.ipc){
			this.ipc.send('font-info',info);
		}
		var index = font.collections.indexOf(this.collection);
		if(index>-1){
			font.collections.splice(index,1);
			font.hide = true;
		}
	}
	onDragOver(evt){
		evt.preventDefault();
	}
	onDrop(evt,collection){
		evt.preventDefault();
		this.addFontToCollection(collection);
	}
	onDragStart(dragging: any){
		this.dragging = dragging;
	}
	searchUpdate(evt){
		this.search = evt;
		this.fontPagination.end = this.pagination;
		this.fontFiltering(evt);
	}
	addStyles(message: string){
		this.zone.run(() => {
			this.stylesheet = message
		});
	}
	updateFontSizeStyles(){
		this.fontSizeStyles = `<style>
			.font-listing .template{
				font-size: ${this.fontSize}px;
			}
		</style>`;
	}
	toggleMenuButton(button: string){
		if(this.activeMenu == button) this.activeMenu = '';
		else this.activeMenu = button;
	}
	pinFont(font: any){
		if(this.pinned && this.pinned.className == font.className){
			this.pinned = false;
			this.marginTop = 0;
		} else {
			this.pinned = font;
			var els = document.getElementsByClassName(font.className);
			if(els.length){
				var height = els[0].parentElement.offsetHeight;
				this.marginTop = height;
			}

		}
	}
	onScroll(evt){
		var bodyHeight = evt.target.scrollHeight;
		var scrollTop = evt.target.scrollTop;
		var windowHeight = window.innerHeight;
		if(this.fontTotal > this.fontPagination.end){
			if((bodyHeight - scrollTop) < windowHeight){
				this.fontPagination.end += this.pagination;
				this.fontFiltering(this.search);
				this.showSpinner = true;
				setTimeout(() =>{
					this.showSpinner = false;
				},500);
			}
		}
	}
	previewUpdate(evt){
		this.previewText = evt;
	}
	setViewMode(mode){
		this.viewMode = mode;
		if(this.ipc){
			this.ipc.send(mode);
		}
	}
	updateFavourite(font){
		font.favourite = !font.favourite;
		if(font.favourite) this.ipc.send('favourite.add',font.postscriptName);
		else this.ipc.send('favourite.remove',font.postscriptName);
		if(!font.favourite && this.collection == 'fav'){
			font.hide = true;
		}
	}
	ngOnInit(){
		this.buildOpenTypeList();
		this.buildCheckBoxes();
	}

	constructor(zone:NgZone){
		this.zone = zone;
		this.updateFontSizeStyles();
		if(typeof require != "undefined"){
			this.ipc = require('electron').ipcRenderer;
			this.ipc.on('message', (event, message) => this.addAllFonts(message));
			this.ipc.on('stylesheet', (event, message) => this.addStyles(message));
			this.ipc.on('collections', (event, message) => this.addCollections(message));
		} else {
			this.addAllFonts([ { path: '/Library/Fonts/Arial.ttf',
				postscriptName: 'Arial',
				className:'arial',
				family: 'Arial',
				style: 'Regular',
				type:'ttf',
				weight: 400,
				width: 5,
				italic: false,
				monospace: false },
				{ path: '/Library/Fonts/Arial Bold.ttf',
					postscriptName: 'Arial Bold',
					className:'arialbold',
					family: 'Arial',
					style: 'Bold',
					type:'ttf',
					weight: 700,
					width: 5,
					italic: false,
					monospace: false } ]);
		}
	}

}