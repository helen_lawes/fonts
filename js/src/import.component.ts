import {Component, Input} from 'angular2/core';
import {Http, HTTP_PROVIDERS} from 'angular2/http';

@Component({
	selector:'import',
	viewProviders: [HTTP_PROVIDERS],
	template:'<div [innerHTML]="html"></div>',
})

export class ImportComponent{
	@Input() url:string;
	private html:string = '';
	private http:any;


	ngOnInit(){
		this.http.get(this.url)
			.map(res => res.text())
			.subscribe(html => this.html = html);
	}
	constructor(http: Http){
		this.http = http;
	}
}