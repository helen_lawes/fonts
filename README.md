# README #

This repo requires electron-prebuilt to be installed:


```
#!js

npm install -g electron-prebuilt
```


then to run use the command


```
#!js

electron .
```


to build install nullsoft scriptable install system from http://nsis.sourceforge.net/Download, then add the folder to system path

run

```
#!js

npm run pack
npm run build
```