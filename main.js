const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipcMain = electron.ipcMain;
const storage = require('electron-json-storage');

var mainWindow = null;
var path = require('path');

var fontManager = require('font-manager');
var appData;

storage.get('fontViewer').then(function(data){
	if(typeof data == "undefined" || !Object.keys(data).length){
		appData = {
			favourites:[],
			collections:[],
			fontInfo:{}
		};
	} else {
		appData = data;
		if(typeof appData.favourites=="undefined"){
			appData.favourites = [];
		}
		if(typeof appData.collections=="undefined"){
			appData.collections = [];
		}
		if(typeof appData.fontInfo=="undefined"){
			appData.fontInfo = {};
		}
	}
});
function saveDown(){
	storage.set('fontViewer', appData);
}
function addRemove(field,name,operation){
	var index = appData[field].indexOf(name);
	if(operation=='add'){
		if(index > -1) {}
		else {
			appData[field].push(name);
			saveDown();
		}
	} else if(operation=='remove') {
		if(index > -1){
			appData[field].splice(index,1);
			saveDown();
		}
	}
}
function createWindow(){
	//mainWindow = new BrowserWindow({width:1280, height:768});
	mainWindow = new BrowserWindow({width:1280, height:768, frame:false});

	mainWindow.loadURL('file://'+ __dirname + '/index.html');

	mainWindow.webContents.on('did-finish-load', function(){
		readFonts();
	});

	mainWindow.on('closed',function(){
		mainWindow = null;
	});

	ipcMain.on('max',function(){
		mainWindow.maximize();
	});
	ipcMain.on('min',function(){
		mainWindow.minimize();
	});
	ipcMain.on('unmax',function(){
		mainWindow.unmaximize();
	});
	ipcMain.on('close',function(){
		mainWindow.close();
	});
	ipcMain.on('favourite.add',function(evt,name){
		addRemove('favourites',name,'add');
	});
	ipcMain.on('favourite.remove',function(evt,name){
		addRemove('favourites',name,'remove');
	});
	ipcMain.on('collection.add',function(evt,name){
		addRemove('collections',name,'add');
	});
	ipcMain.on('collection.remove',function(evt,name){
		addRemove('collections',name,'remove');
	});
	ipcMain.on('font-info',function(evt,info){
		var fontClass = info.fontClass;
		var fontCollection = info.fontCollection;
		var add = info.add;

		var fontInfo = appData.fontInfo[fontClass];
		if(add){
			if(typeof fontInfo != "undefined"){
				if(fontInfo.indexOf(fontCollection)>-1){
					appData.fontInfo[fontClass].push(fontCollection);
					saveDown();
				}
			} else {
				appData.fontInfo[fontClass] = [fontCollection];
				saveDown();
			}
		} else {
			if(typeof fontInfo != "undefined"){
				var index = fontInfo.indexOf(fontCollection);
				if(index>-1){
					appData.fontInfo[fontClass].splice(index,1);
					saveDown();
				}
			}
		}
	});

}

function readFonts(){
	fontManager.getAvailableFonts(function(fonts){
		fonts.sort(function(a,b){
			var aName = a.postscriptName.toLowerCase();
			var bName = b.postscriptName.toLowerCase();
			if(aName < bName) return -1;
			if(aName > bName) return 1;
			return 0;
		});
		var stylesheet = "<style>";
		fonts.forEach(function(font,i){
			var parts = path.parse(font.path);
			font.postscriptName = font.postscriptName.replace(/-+/g,' ').replace(/mt/i,'').trim();
			font.className = font.postscriptName.replace(/[^a-zA-Z]/g,'');
			font.path = font.path.replace(/\\/g,'/');
			font.type = parts.ext.toLowerCase().replace(/\./,'').trim();
			font.favourite = (appData.favourites.indexOf(font.postscriptName) > -1);
			var fontInfo = appData.fontInfo[font.className];
			font.collections = (typeof fontInfo == "undefined") ? [] : fontInfo;
			fonts[i] = font;
			stylesheet += '@font-face { font-family: "'+font.className+'"; src: url("'+font.path+'"); } .'+font.className+'{ font-family: "'+font.className+'"; }';
		});
		stylesheet += "</style>";
		mainWindow.webContents.send('message',fonts);
		mainWindow.webContents.send('stylesheet',stylesheet);
		mainWindow.webContents.send('collections',appData.collections);
	});
}

app.on('window-all-closed',function(){
	if (process.platform != 'darwin') {
		app.quit();
	}
});

app.on('ready',createWindow);

app.on('activate', function(){
	if(mainWindow === null){
		createWindow();
	}
});